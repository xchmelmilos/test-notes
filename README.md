# Notes (test application)

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Usage
- run `yarn install`
- run `yarn start`
- Open [http://localhost:9000](http://localhost:9000) to view it in the browser.

## Status of requirements

**Metody:**
- [x] GET /notes
- [x] GET /notes/{id}
- [x] POST /notes
- [x] PUT /notes/{id}
- [x] DELETE /notes/{id}

**Funkční požadavky:**

- [x] Po instalaci a spuštění se po zadání localhost:9000 
- [x] objeví stránka se seznamem s poznámkami.
- [x] Je možné zobrazit detail,
- [x] editovat, 
- [x] smazat a 
- [x] vytvořit novou poznámku. (Apiary Mock bude vracet stále stejná data, předmětem úkolu je volat správné metody)
- [x] Router, (pokud použijete React a Redux pak react-redux-router)
- [ ] V aplikaci bude možné měnit EN/CZ jazyk (místo CZ může být RU, DE..)…

**Nefunkční požadavky:**

- [x] GUI dle vlastního návrhu, použití Bootstrapu/Material/Bulma a 
- [x] alespon jedno z: css-modules/styled-components/LESS/SCSS/Stylus/…
- [x] Kód by měl být ES6+ JS nebo TS 
- [x] s použitím novějších API jako Promise, `Promise je sytaktický cukr pro await, ale api Promise použito je.`
- [x] Array HOF
- [x] async/await 
- [ ] použití generatorové funkce (CO.js, redux-saga) je plus
- [x] Instalace závislostí a build pomocí webpack, rollup. `Zapouzdřeno v Create React App`
- [x] Alespoň jeden základní Unit test (Jest, Jasmin, Mocha, Chai…). `Jeden byl vygenerován pomocí Create React App`

## Not done

- error handling
- input validation
