import React from "react";
import {BrowserRouter as Router, Route, Switch} from "react-router-dom";
import {NoteList} from "./pages/NoteList";
import {NoteDetail} from "./pages/NoteDetail";

export const App = () => {
    return (
        <Router>
            <Switch>
                <Route path="/note/:note_id">
                    <NoteDetail/>
                </Route>
                <Route path="/">
                    <NoteList/>
                </Route>
            </Switch>
        </Router>
    )
};
