import React from 'react';
import ReactDOM from 'react-dom';
import {NoteList} from './pages/NoteList';
import {NoteDetail, NoteDetailPure} from "./pages/NoteDetail";
import {App} from "./App";
import {Router} from "react-router";

it('detects exception/crash', () => {

    // FIXME: this should throw too. Is it optimized away?
    // let a = 10;
    // let b = 0;
    // const throwsExeption = () => a / b;

    const throwsExeption = () => {
        throw new Error('exception');
    };
    expect(throwsExeption).toThrow();

    const doesNotThrow = () => {
    };
    expect(doesNotThrow).not.toThrow();
});

describe('Pages do not crash', () => {
    it('renders NotesList without crashing', () => {
        const div = document.createElement('div');
        ReactDOM.render(<NoteList/>, div);
        ReactDOM.unmountComponentAtNode(div);
    });

    // const SimulateUrl = (url) => {
    //     global.window = Object.create(window);
    //     Object.defineProperty(window, 'location', {
    //         value: {
    //             href: url
    //         }
    //     });
    // };

    it('renders NoteDetail without crashing', () => {

        // SimulateUrl('/notes/1' );

        const div = document.createElement('div');
        ReactDOM.render(<NoteDetailPure history={null} isLoaded={false} note={{title: 'test', id: '1'}} setNote={() => {}}/>, div);
        ReactDOM.unmountComponentAtNode(div);
    });
});
