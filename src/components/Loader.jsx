import {Row, Spinner} from "react-bootstrap";
import {t} from "../utils/translation";
import React from "react";


const Centered = ({children}) => {
    return (
        <Row className={'align-items-center justify-content-center'}>
            {children}
        </Row>
    )
};

const LoaderInner = () => (
    <Spinner animation="border" role="status">
        <span className="sr-only">{t('Načítám...')}</span>
    </Spinner>
);

export const Loader = () => {
    return (
        <Centered>
            <LoaderInner/>
        </Centered>
    )
};
