import React from 'react';
import {t} from "../utils/translation";
import {Button, ButtonToolbar, Col, Container, Row} from "react-bootstrap";
import {useHistory} from "react-router-dom";

export const NoteItem = ({id, title, deleteNote}) => {

    let history = useHistory();

    const GoToNote = (note_id) => {
        history.push(`/note/${note_id}`);
    };

    return (
        <Row>
            <Col sm={1}>
                {id}
            </Col>
            <Col>
                <Container>
                    <Row>
                        <b className={'flex-grow-1'}>{title}</b>
                        <ButtonToolbar>
                            {/*<Link to={}>{t('Uprav')}</Link>*/}
                            <Button onClick={() => {
                                GoToNote(id);
                            }}>{t('Uprav')}</Button>
                            &nbsp;
                            <Button variant={'outline-secondary'} onClick={deleteNote}>{t('Smaž')}</Button>
                        </ButtonToolbar>
                    </Row>
                </Container>
            </Col>
        </Row>
    );
};
