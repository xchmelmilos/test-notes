import React, {useEffect, useState} from "react";
import {Button, Col, Container, FormControl, InputGroup} from "react-bootstrap";

import {t} from "../utils/translation";
import {useHistory, useParams} from "react-router";
import {Loader} from "../components/Loader";

async function UpdateNote(id, title) {
    await fetch(`http://private-9aad-note10.apiary-mock.com/notes/${id}`, {
        method: "PUT",
        headers: {
            'content-type': 'application/json'
        },
        body: JSON.stringify({title})
    });
}

async function GetNote(id) {
    let response = await fetch(`http://private-9aad-note10.apiary-mock.com/notes/${id}`);
    let data = await response.json();
    return data;
}

export function NoteDetailPure({history, note_id, isLoaded, setNote, note}) {
    return (
        <Container>
            <Col>
                <Button variant="outline-secondary"
                        onClick={() => {
                            history.push('/');
                        }}
                >{t('Zpět')}</Button>

                <h2>{t('Úprava poznámky')}&nbsp;{note_id}</h2>

                {!isLoaded ?
                    <Loader/>
                    :
                    (
                        <React.Fragment>
                            <label htmlFor={'text'}>{t('Text')}</label>
                            <InputGroup>
                                <FormControl id={'text'}
                                             onChange={(event) => {
                                                 setNote({...note, title: event.target.value});
                                             }}
                                             value={note.title}
                                />
                                <InputGroup.Append>
                                    <Button onClick={() => UpdateNote(note_id, note.title)}>{t('Upravit')}</Button>
                                </InputGroup.Append>
                            </InputGroup>
                        </React.Fragment>
                    )
                }
            </Col>
        </Container>
    )
}

export const NoteDetail = () => {

    let {note_id} = useParams();
    let history = useHistory();

    let [note, setNote] = useState({});
    let [isLoaded, setIsLoaded] = useState(false);
    useEffect(() => {
        GetNote(note_id).then((note) => {
            setNote(note);
            setIsLoaded(true);
        });
    }, [note_id]);

    return (
        <NoteDetailPure history={history} note_id={note_id} isLoaded={isLoaded} setNote={setNote} note={note}/>
    );
};
