import React from 'react';
import {NoteItem} from "../components/NoteItem";
import {t} from "../utils/translation";
import {Button, Container, FormControl, InputGroup, Jumbotron} from 'react-bootstrap';
import Alert from "react-bootstrap/Alert";
import {Loader} from "../components/Loader";

async function LoadNotes() {
    let notesRequest = await fetch('http://private-9aad-note10.apiary-mock.com/notes');
    let notes = await notesRequest.json();
    return notes;
}

async function DeleteNote(noteId) {
    await fetch(`http://private-9aad-note10.apiary-mock.com/notes/${noteId}`, {method: "DELETE"});
}

async function AddNote(title) {
    await fetch(`http://private-9aad-note10.apiary-mock.com/notes`, {
        method: "POST",
        headers: {
            'content-type': 'application/json'
        },
        body: JSON.stringify({title})
    });
}

const ErrorMessage = ({title, text}) => {
    return (
        <Alert dismissible variant="danger">
            <Alert.Heading>{title}</Alert.Heading>
            <p>
                {text}
            </p>
        </Alert>
    )
};


export class NoteList extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            notes: [],

            text: ''
        };
    }

    componentDidMount() {
        LoadNotes().then((notes) => {
            this.setState({notes});
        });
    }

    render() {

        let isNotesLoading = this.state.notes.length === 0;

        return (
            <React.Fragment>

                <Jumbotron className="text-center">
                    <Container>
                        <h1>{t('Poznámky')}</h1>
                        <p>{t('Si nemusíte nechat doma.')}</p>
                    </Container>
                </Jumbotron>

                <Container>

                    <h2>{t('Výpis poznámek')}</h2>
                    {isNotesLoading ?
                        <Loader/>
                        :
                        this.state.notes.map(({id, title}) => (
                            <NoteItem
                                key={id}
                                id={id}
                                title={title}
                                deleteNote={() => DeleteNote(id)}
                            />))
                    }

                    <h2>{t('Nová poznámka')}</h2>
                    <label htmlFor={'text'}>{t('Text')}</label>
                    <InputGroup>
                        <FormControl id={'text'}
                                     onChange={(event) => this.setState({text: event.target.value})}
                        />
                        <InputGroup.Append>
                            <Button onClick={() => AddNote(this.state.text)}>{t('Přidat')}</Button>
                        </InputGroup.Append>
                    </InputGroup>
                </Container>
            </React.Fragment>
        );
    }
}
