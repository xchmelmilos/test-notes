// TODO: are non-ascii symbols problem?
let translation = {
    "Zpět": {
        "cs": "Zpět",
        "en": "Back"
    },
    "Úprava poznámky": {
        "cs": "Úprava poznámky",
        "en": "Editing note"
    },
    "Načítám...": {
        "cs": "Načítám...",
        "en": "Loading..."
    },
    "Text": {
        "cs": "Text",
        "en": "Text"
    },
    "Upravit": {
        "cs": "Upravit",
        "en": "Edit"
    },
    "Poznámky": {
        "cs": "Poznámky",
        "en": "Notes"
    },
    "Si nemusíte nechat doma.": {
        "cs": "Si nemusíte nechat doma.",
        "en": "(Insert funny subtext)"
    },
    "Výpis poznámek": {
        "cs": "Výpis poznámek",
        "en": "Note list"
    },
    "Nová poznámka": {
        "cs": "Nová poznámka",
        "en": "New note"
    },
    "Přidat": {
        "cs": "Přidat",
        "en": "Add"
    },
    "Uprav": {
        "cs": "Uprav",
        "en": "Edit"
    },
    "Smaž": {
        "cs": "Smaž",
        "en": "Delete"
    }
};

let collected_translation = {};

export function t(translateText) {

    // M: does nothing
    // return translateText;

    // M: quick & dirty translation gathering
    // collected_translation[translateText] = {cs: translateText, en: '???'};
    // console.log({collected_translation});

    let isoLanguage = 'cs'; // TODO: get from route
    let translatedText = translation[translateText][isoLanguage];
    return translatedText;
}
